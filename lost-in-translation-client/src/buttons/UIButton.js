import { useState } from "react";

export default function UIButton(props) {
  const [searchValue, setSearchValue] = useState("");

  const handleInputChange = (e) => {
    setSearchValue(e.target.value);
  };

  const handleLoginClick = () => {
    props.onClick();
    setSearchValue("");
  };

  const handleClearInputOnClick = () => {
    setSearchValue("");
  };

  return (
    <div className="UI-button">
      <button
        style={{
          padding: "2px",
          textDecoration: "none",
          height: "20px",
          fontSize: "10px",
        }}
        className="buttonStyle"
        value={props.value}
        onClick={handleLoginClick}
        onChange={handleInputChange}
      >
        {props.name}
      </button>
    </div>
  );
}
