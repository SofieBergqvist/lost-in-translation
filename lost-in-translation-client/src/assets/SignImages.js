import React from "react";

function SignImages(props) {
  const translated = [...props.translate].map((character, index) => {
    if (!character.match(/[^a-zA-Z ]+$/, "")) {
      return (
        <img key={index} src={`assets/${character}.png`} alt={character} />
      );
    } else {
      return <span key={index}></span>;
    }
  });

  return <div>{translated}</div>;
}

export default SignImages;
