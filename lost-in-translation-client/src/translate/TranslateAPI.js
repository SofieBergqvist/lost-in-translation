const TRANSLATION_URL = "http://localhost:5000";

export function checkTranslation(translation) {
  return fetch(`${TRANSLATION_URL}/translation`)
    .then((r) => r.json())
    .then((translation) => {
      return translation.find((u) => u.translation === translation);
    });
}

export function createTranslation(translation) {
  return fetch(`${TRANSLATION_URL}/translation`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ translation }),
  }).then((r) => r.json());
}

export function getTranslation() {
  return fetch(`${TRANSLATION_URL}/translation`)
    .then((response) => response.json())
    .then((data) => {
      const output = data
        .map((t) => {
          return ` ${t.translation},`;
        })
        .slice(-10)
        .join("");
      console.log(output);

      return output;
    });
}

/*

export function displayTranslations(translation) {
  return fetch(`${TRANSLATION_URL}/translation`)
    .then((response) => response.json())

    .then((translation) => {
      // Cause Side Effects.
      return translation.slice(-10);
    });
}

export function getTranslation(translation) {
  return fetch(`${TRANSLATION_URL}/translation`)
    .then((response) => response.json())
    .then((data) => {
      let output = "";
      data.forEach((t) => {
        output += `
       ${t.translation}
      `;
      });
      return output;
    });
}
*/
