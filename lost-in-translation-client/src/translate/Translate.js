import React, { useContext } from "react";
import { Redirect } from "react-router-dom";
import { useState } from "react";
import { createTranslation } from "./TranslateAPI";
import { AppContext } from "../global/AppProvider";
import TranslateForm from "./TranslateForm";
import SignImages from "../assets/SignImages";
import { getStorage, setStorage } from "../utils/storage";

const Translation = (props) => {
  const { translation, setTranslation } = useContext(AppContext);
  const [inputError, setInputError] = useState(false);
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [newTranslation, setNewTranslation] = useState(false);

  const handleTranslateClicked = async (e) => {
    setTranslation(e);
    try {
      const newTranslation = await createTranslation(translation);
      if (newTranslation) {
        setInputError(false);
      }
    } catch (error) {
      console.log(error.message);
    }
    setNewTranslation(true);
  };

  return (
    <div>
      <div>
        {!isLoggedIn && <Redirect to="/" />}
        <TranslateForm translateClicked={handleTranslateClicked} />
        <div style={{ width: "80%", marginLeft: "8em" }}>
          {newTranslation && <SignImages translate={translation} />}
        </div>
      </div>
    </div>
  );
};

export default Translation;
