import React, { useContext, useState } from "react";
import Navbar from "../navbar/Navbar";
import { Link } from "react-router-dom";
import { AppContext } from "../global/AppProvider";
import "./TranslateForm.module.css";

export const TranslateForm = (props) => {
  const { username, translation, setTranslation } = useContext(AppContext);
  const [loginError, setLoginError] = useState("");

  const onInputChanged = (e) => {
    setTranslation(e.target.value.trim());
  };

  const handleOnTranslateClick = () => {
    if (translation == "") {
      setLoginError("Please enter text");
    }
    props.translateClicked(translation);
  };

  return (
    <div className="container-translate">
      <Navbar username={username} />
      <form className="form">
        <div>
          <input
            className="input-searchField"
            type="search"
            placeholder="Enter text.."
            onChange={onInputChanged}
            maxLength="40"
          ></input>
          {loginError && (
            <small
              style={{
                display: "block",
                color: "red",
              }}
            >
              {loginError}
            </small>
          )}
        </div>
        <button
          style={{ height: "60px" }}
          className="translateButton"
          type="button"
          onClick={handleOnTranslateClick}
        >
          Translate
        </button>
        <Link to="/profile">
          <button style={{ height: "60px" }} className="buttonStyle">
            Go to Profile
          </button>
        </Link>
      </form>
    </div>
  );
};

export default TranslateForm;
