import React from "react";
import { Link } from "react-router-dom";

function NotFound() {
  <div>
    <h1>Page not found</h1>
    <Link to="/">Redirect to home</Link>
  </div>;
}
export default NotFound;
