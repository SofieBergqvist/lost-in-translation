import { createContext, useState } from "react";

export const AppContext = createContext();

function AppProvider(props) {
  const [username, setUsername] = useState([]);
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [translation, setTranslation] = useState([]);

  const initialAppState = {
    username,
    setUsername,
    isLoggedIn,
    setLoggedIn,
    translation,
    setTranslation,
  };

  return (
    <AppContext.Provider value={initialAppState}>
      {props.children}
    </AppContext.Provider>
  );
}

export default AppProvider;
