import { useState } from "react";
import Navbar from "../navbar/Navbar";
import { checkUsername, createUsername } from "./LoginAPI";
import { setStorage } from "../utils/storage";
import "./LoginForm.module.css";
import { Redirect } from "react-router-dom";
import { useContext } from "react";
import { AppContext } from "../global/AppProvider";

function LoginForm() {
  const { username, setUsername, isLoggedIn, setLoggedIn } = useContext(
    AppContext
  );
  const [loginError, setLoginError] = useState("");

  const handleLoginClick = async (e) => {
    e.preventDefault();

    if (!username) {
      setLoginError("Please enter your name!");
      return;
    }

    try {
      const registeredUser = await checkUsername(username);
      if (registeredUser) {
        setStorage("_login-session", username);
        setLoggedIn(true);
        return;
      }

      const createdUser = await createUsername(username);
      if (createdUser) {
        setLoggedIn(true);
        setStorage("_login-session", username);
        console.log("created user?", typeof username, "username");
      }
      //props.clickLogin(username);
    } catch (error) {
      console.log(error.message);
    }
  };
  if (isLoggedIn) {
    return <Redirect to="translation" />;
    //FIX --IF NOT LOGGED IN BUTTON DISPLAY false
  }

  //FIX-- ADD A PAGE NOT FOUND PAGE
  const onUsernameChanged = (e) => setUsername(e.target.value.trim());

  const inline = {
    display: "inline",
  };

  return (
    <div>
      <form>
        <h1 className="title-login" style={{ display: "block" }}>
          Login
        </h1>
        <input
          type="text"
          autoComplete="off"
          placeholder="Enter username"
          maxLength="40"
          onChange={onUsernameChanged}
        />
        <span>
          <button
            style={inline}
            type="button"
            className="btnLogin"
            onClick={handleLoginClick}
          >
            Login here
          </button>
        </span>

        <div className="error-msg">
          {loginError && <bold style={{ color: "red" }}>{loginError}</bold>}
        </div>
        {isLoggedIn && <Navbar username={username}></Navbar>}
      </form>
    </div>
  );
}

export default LoginForm;
