import { useContext, useEffect, useState } from "react";
import { checkUsername, createUsername } from "./LoginAPI";
import { getStorage, setStorage } from "../utils/storage";
import { Link } from "react-router-dom";
import LoginForm from "./LoginForm";
import { AppContext } from "../global/AppProvider";

function Login() {
  const username = getStorage("_login-session");
  const { setUsername, isLoggedIn, setLoggedIn } = useContext(AppContext);

  const handleLoginClicked = (username) => {
    setStorage("_login-session", username);
    setUsername(username);
  };

  const onUserSubmit = (e) => {
    handleLoginClicked(e);
  };

  return (
    <div>
      {username && <Link to="/translation" />}
      <LoginForm onUserSubmit={onUserSubmit} />
    </div>
  );
}

export default Login;
