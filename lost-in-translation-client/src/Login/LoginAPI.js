const URL = "http://localhost:5000";

export function checkUsername(username) {
  return fetch(`${URL}/users`)
    .then((r) => r.json())
    .then((users) => {
      return users.find((u) => u.username === username);
    });
}

export function createUsername(username) {
  return fetch(`${URL}/users`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username }),
  }).then((r) => r.json());
}

export function getUsername(username) {
  return (
    fetch(`${URL}/users`)
      //.then((response) => response.json())
      .then((username) => {
        return username[username.length - 1];
      })
  );
}
