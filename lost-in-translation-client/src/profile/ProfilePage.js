import { useHistory } from "react-router-dom";
import { useContext, useEffect } from "react";
import { AppContext } from "../global/AppProvider";
import { displayTranslations, getTranslation } from "../translate/TranslateAPI";
import Navbar from "../navbar/Navbar";

function ProfilePage() {
  const { username, translation, setTranslation } = useContext(AppContext);

  useEffect(() => {
    getTranslation()
      .then((translation) => {
        console.log("TranslationHistory.useEffect:", translation);
        setTranslation(translation);
      })
      .catch((error) => {
        console.error("TranslationHistory.useEffect:", error);
      });
  }, []);

  const history = useHistory();

  const backToTranslation = () => {
    history.push("/translation");
  };

  return (
    <>
      <Navbar username={username} />
      <div style={{ width: "80%" }}>
        <h2>Your last translations:</h2>
        <div
          style={{
            fontStyle: "italic",
            textAlign: "center",
            width: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="Container-translation"
        >
          <div
            style={{
              fontStyle: "italic",
              textAlign: "center",
              width: "50%",
            }}
          >
            {translation}
          </div>
        </div>
        <article style={{ width: "60%", padding: "2em" }}>
          <button
            style={{ display: "block", float: "right" }}
            className="back-button"
            onClick={backToTranslation}
          >
            Back to translation
          </button>
        </article>
      </div>
    </>
  );
}

export default ProfilePage;
