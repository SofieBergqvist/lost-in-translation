import { useContext, useEffect, useState } from "react";
import { AppContext } from "../global/AppProvider";
import { logout } from "../utils/storage";
import { Link } from "react-router-dom";
import UIButton from "../buttons/UIButton";
import LoginForm from "../Login/LoginForm";
import { getUsername } from "../Login/LoginAPI";

function Navbar(props) {
  const { username, setUsername, setLoggedIn } = useContext(AppContext);
  const [btnLogOut, setBtnLogOut] = useState(false);

  /*
  useEffect(() => {
    getUsername()
      .then((username) => {
        console.log("UsernameHistory.useEffect:", username);
        getUsername(props.username);
        return username;
      })
      .catch((error) => {
        console.log(error);
      });
  });
*/
  const handleLogOutClick = () => {
    setLoggedIn(false);
    setUsername("");
    localStorage.removeItem("username");
    setBtnLogOut(!btnLogOut);
  };

  const userLoggedIn = (username) => {
    setUsername = <LoginForm username={username} />;
    return userLoggedIn;
  };

  return (
    <div className="container-nav">
      <header>
        <nav>
          <h1 style={{ fontStyle: "italic" }} className="title-nav">
            Lost in Translation
          </h1>
          <p className="paragraph">Logged in as: {props.username}</p>
          <div className="change-to-Router-Link"></div>
          <Link to="/">
            <UIButton
              className="buttonStyle"
              name="Log out"
              value={btnLogOut}
              onClick={handleLogOutClick}
            />
          </Link>
        </nav>
      </header>
    </div>
  );
}

export default Navbar;
