import "./App.css";
import Login from "./Login/Login";
import Translate from "./translate/Translate";
import ProfilePage from "./profile/ProfilePage";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={Login}></Route>
          <Route exact path={"/translation"} component={Translate}></Route>
          <Route exact path={"/profile"} component={ProfilePage}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
